/* QUOTES SECTION */

var slideShowIndex=0;
showSlides();

function showSlides(){
    
    var i;
    var slides=document.getElementsByClassName("mySlides");
    var dots=document.getElementsByClassName("dot");
    for(i=0;i<slides.length;i++){
        
        slides[i].style.display="none";
        
    }
    
    slideShowIndex++;
    
    if(slideShowIndex>slides.length){
        
        slideShowIndex=1;
        
    }
    
    for(i=0;i<dots.length;i++){
        
        dots[i].className=dots[i].className.replace("active"," ");
        
    }
    
    slides[slideShowIndex-1].style.display="block";
    dots[slideShowIndex-1].className += "active";
    
    setTimeout(showSlides,5000);
    
}

/* TOP 5 SECTION */

var textSlideIndex=1;
textShowSlides(textSlideIndex);

function plusSlides(n){
    
    textShowSlides(textSlideIndex+=n);
    
}

function textShowSlides(n){
    
    var i;
    var textSlides=document.getElementsByClassName("mytext");
    
    if(n>textSlides.length){
        
        textSlideIndex=1;
        
    }
    
    if(n<1){
        
        textSlideIndex=textSlides.length;
        
    }
    
    for(i=0;i<textSlides.length;i++){
        
        textSlides[i].style.display="none";
        
    }
    
    textSlides[textSlideIndex-1].style.display="block";
    
}

/* SIDE NAVIGATION BAR */

function openNav(){
    document.getElementById("res-side-nav").style.display="block";
    document.getElementById("res-side-nav").style.width="250px";
    document.body.style.backgroundColor="rgba(0,0,0,0.4)";
}

function closeNav(){
    document.getElementById("res-side-nav").style.display="none";
    document.getElementById("res-side-nav").style.width="0";
    document.body.style.backgroundColor="white";
    
}